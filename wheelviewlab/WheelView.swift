//
//  WheelView.swift
//  wheelviewlab
//
//  Created by Jin Hong on 2019-07-25.
//  Copyright © 2019 junk. All rights reserved.
//

import UIKit

class WheelView: RotationControl {

    override var rotation: CGFloat? {
        get { return super.rotation }
    }

    override func draw(_ layer: CALayer, in ctx: CGContext) {
        super.draw(layer, in: ctx)
        let dialCount = 60
        (0..<dialCount).forEach { i in
            drawDial(rotatedBy: .pi / CGFloat(dialCount / 2), emphasized: i % 15 == 0, in: ctx)
        }
    }

    func drawDial(rotatedBy rotation: CGFloat, emphasized: Bool, in ctx: CGContext) {
        let width: CGFloat = emphasized ? 4 : 2
        let height: CGFloat = emphasized ? 75 : 60
        let xOffset: CGFloat = emphasized ? 2 : 1
        let color: CGColor = emphasized ? UIColor.red.cgColor : UIColor.purple.cgColor
        ctx.setFillColor(color)
        ctx.fill(.init(x: bounds.midX - xOffset, y: 10, width: width, height: height))
        ctx.translateBy(x: bounds.width / 2, y: bounds.height / 2)
        ctx.rotate(by: rotation)
        ctx.translateBy(x: -bounds.width / 2, y: -bounds.height / 2)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        layer.cornerRadius = frame.width / 2
        layer.masksToBounds = true
    }
}
