//
//  RotationControl.swift
//  wheelviewlab
//
//  Created by Jin Hong on 2019-07-25.
//  Copyright © 2019 junk. All rights reserved.
//

import UIKit

class Control: UIControl {
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        translatesAutoresizingMaskIntoConstraints = false
    }
}

private extension CGVector {
    static func angleBetween(_ lhs: CGVector, _ rhs: CGVector) -> CGFloat {
        let angle = atan2(lhs.dy, lhs.dx) - atan2(rhs.dy, rhs.dx)
        return angle > 0 ? angle : (2 * .pi) + angle
    }
}

fileprivate enum Direction {
    case clockwise(CGFloat), anticlockwise(CGFloat)
    init(_ angle: CGFloat) {
        switch angle > .pi {
        case true: self = .anticlockwise(-2 * .pi + angle)
        case false: self = .clockwise(angle)
        }
    }

    var angle: CGFloat {
        switch self {
        case .clockwise(let angle): return angle
        case .anticlockwise(let angle): return angle
        }
    }
}

fileprivate struct Velocity {
    var magnifiedAngle: CGFloat { return direction.angle * magnitude }
    let magnitude: CGFloat
    let direction: Direction

    init(_ cv: CGVector, _ pv: CGVector) {
        direction = Direction(CGVector.angleBetween(cv, pv))
        magnitude = Velocity.calculateMagnitude(vectorDelta: cv - pv)
    }

    private init(_ velocity: Velocity, _ angle: CGFloat,_  damping: CGFloat) {
        switch velocity.direction {
        case .clockwise: direction = .clockwise(angle)
        case .anticlockwise: direction = .anticlockwise(-angle)
        }
        magnitude = velocity.magnitude * damping
    }

    private static func calculateMagnitude(vectorDelta: CGVector) -> CGFloat {
        let movement = abs(vectorDelta.dx) + abs(vectorDelta.dy)
        let magnitude = min(movement, 100) * .pi / 100
        return magnitude
    }

    static func easing(_ velocity: Velocity, _ angle: CGFloat, _ damping: CGFloat) -> Velocity {
        return Velocity(velocity, angle, damping)
    }
}

class RotationControl: Control {

    // MARK: constants
    private let decelerationAngle: CGFloat = .pi / 32
    private let decelerationDampingRate: CGFloat = 0.95
    private let decelerationThreshold: CGFloat = 0.25
    private let decelerationInterval: TimeInterval = 1/120

    // MARK: state
    private(set) var rotation: CGFloat?
    private var velocity: Velocity?
    private var trackedVector: CGVector?

    // MARK: touch tracking
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        trackedVector = touch.location(in: self) - bounds.centerPoint
        sendActions(for: .editingDidBegin)
        return super.beginTracking(touch, with: event)
    }

    override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let currentVector = touch.location(in: self) - bounds.centerPoint
        defer { trackedVector = currentVector }
        guard let previousVector = trackedVector else { return false }
        velocity = Velocity(currentVector, previousVector)
        rotation = rotation.map { $0 + (velocity?.magnifiedAngle ?? 0) } ?? velocity?.magnifiedAngle
        sendActions(for: .valueChanged)
        return super.continueTracking(touch, with: event)
    }

    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        super.endTracking(touch, with: event)
        deferSendActions(for: .editingDidEnd)
    }

    override func cancelTracking(with event: UIEvent?) {
        super.cancelTracking(with: event)
        deferSendActions(for: .touchCancel)
    }

    // MARK: deceleration
    private func deferSendActions(for events: UIControl.Event) {
        Timer.scheduledTimer(timeInterval: decelerationInterval, target: self, selector: #selector(decelerate), userInfo: events, repeats: true)
    }

    @objc private func decelerate(_ sender: Timer) {
        guard
            let events = sender.userInfo as? UIControl.Event
            else { return willEndDecelerating(for: .editingDidEnd, timer: sender) }
        let shouldDecelerate = velocity.map { $0.magnitude > decelerationThreshold } == true
        return shouldDecelerate ? updateDecelerationState() : willEndDecelerating(for: events, timer: sender)
    }

    private func willEndDecelerating(for events: UIControl.Event, timer: Timer) {
        velocity = nil
        trackedVector = nil
        timer.invalidate()
        sendActions(for: events)
    }

    private func updateDecelerationState() {
        velocity = velocity.map { Velocity.easing($0, decelerationAngle, decelerationDampingRate) }
        rotation = rotation.map { $0 + (velocity?.magnifiedAngle ?? 0) }
        sendActions(for: .valueChanged)
    }

    // MARK: drawing
    override func draw(_ rect: CGRect) {
        //
    }

    override func draw(_ layer: CALayer, in ctx: CGContext) {
        super.draw(layer, in: ctx)
        // rotate about the center
        rotation.map {
            ctx.translateBy(x: bounds.width / 2, y: bounds.height / 2)
            ctx.rotate(by: $0)
            ctx.translateBy(x: -bounds.width / 2, y: -bounds.height / 2)
        }
    }
}
