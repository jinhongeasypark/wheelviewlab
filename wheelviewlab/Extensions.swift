//
//  Extensions.swift
//  wheelviewlab
//
//  Created by Jin Hong on 2019-07-25.
//  Copyright © 2019 junk. All rights reserved.
//

import UIKit

extension UIView {
    func fill(parentView view: UIView) {
        topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }

    func center(inParentView view: UIView) {
        centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}

extension CGPoint {
    static func -(_ lhs: CGPoint, _ rhs: CGPoint) -> CGVector {
        return CGVector(dx: lhs.x - rhs.x, dy: lhs.y - rhs.y)
    }
}

extension CGVector {
    static func -(_ lhs: CGVector, _ rhs: CGVector) -> CGVector {
        return CGVector(dx: lhs.dx - rhs.dx, dy: lhs.dy - rhs.dy)
    }
}

extension CGRect {
    var centerPoint: CGPoint {
        return CGPoint(x: origin.x + size.width / 2, y: origin.y + size.height / 2)
    }
}

extension CGFloat {
    var printDescription: String {
        return String(format: "%.5f", self)
    }
}
