//
//  ViewController.swift
//  wheelviewlab
//
//  Created by Jin Hong on 2019-07-05.
//  Copyright © 2019 junk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let wheelView = WheelView(frame: .zero)

    deinit {
        wheelView.removeTarget(self, action: nil, for: [.editingDidBegin, .valueChanged, .editingDidEnd, .touchCancel])
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let diameter: CGFloat = 350
        addRectangle(toView: wheelView)
        view.addSubview(wheelView)
        wheelView.center(inParentView: view)
        wheelView.widthAnchor.constraint(equalToConstant: diameter).isActive = true
        wheelView.heightAnchor.constraint(equalToConstant: diameter).isActive = true
        wheelView.backgroundColor = .yellow
        wheelView.addTarget(self, action: #selector(controlValueChanged), for: .valueChanged)
        wheelView.addTarget(self, action: #selector(controlDidEnd), for: [.editingDidEnd, .touchCancel])
    }

    private func addRectangle(toView view: UIView) {
        let v = UIView(frame: .zero); v.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(v)
        v.center(inParentView: view)
        v.widthAnchor.constraint(equalToConstant: 30).isActive = true
        v.heightAnchor.constraint(equalToConstant: 60).isActive = true
        v.backgroundColor = .black
    }

    @objc private func controlValueChanged(_ sender: WheelView) {
        sender.setNeedsDisplay()
    }

    @objc private func controlDidEnd(_ sender: WheelView) {
        sender.rotation.map {
            print("FINISHED WITH DEGREES:", 180 * $0 / .pi)
        }
    }
}
